
# Repository configuration

In order to use gitlab in the best efficient way, all projects should be configured in the same way.

## "Settings" > "General"

In the "Merge request" section (in the "merge method" sub section), please ensure that "Fast-forward merge" is selected.

> This settings ensure us that every merge request is up to date with the source branch before the merge.

![Merge request method](./assets/mr_method.png "Merge request - merge method configuration")

## "Settings" > "Repository"

### Default branch

> We should have 2 *long lived* branches : 
> - `master`
> - `develop`

In the "Default branch" section, select `develop`.

![Default branch](./assets/gitlab_repo_default_branch.png "Default branch")

### Protected branches

In the "Protected branches" section, define `master` and `develop` as protected branches, with the following options : 
- Master
  - Allowed to merge : Maintainers
  - Allowed to push : No one
- Develop
  - Allowed to merge : Developers + Maintainers
  - Allowed to push : Maintainers
  - Allowed to force push

![Protected branches](./assets/gitlab_repo_protected.PNG "Protected branches")


