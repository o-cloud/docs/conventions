
# Conventions

The goal of this repository is to centralize information about conventions, best
practices and how to.

# Overview

## Configuration

- [Gitlab - Repository configuration](./repository_configuration.md) : How to properly configure a gitlab repository

## GoLang

- [Code review - Best practices](./review.md) : How to review Golang merge requests
- [Effective Go](./effective_go.md) : How to write Golang code efficiently
- [Microservice structure](./microservice_structure.md) : How to structure a microservice
