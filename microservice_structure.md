
# Microservice repository structure

## Microservice Template

A basic go microservice (Boilerplate) is available here https://sb-forge.pf.irt-saintexupery.com/paul.mirial/go-template.
This example project use:
- Libraries
    - Gin Framework (rest)
    - Mongodb (db)
    - Viper (config)
- Continuous Development (Skaffold)
- Continuous Integration (gitlab CI)

## Documentation

A `README.md` file must be present at the root of the repository. This file should be
strucruted like this : 

```markdown
# <Microservice Name>
- Short description

## Functional overview
- What is it for ?
- Why we use this and not another solution ?
- What are other solution tried ?
- ...

## Technical overview
- Prerequisites ?
- How to use ?
- ...
```

The functional overview will also be used in order to build the "CIR" (Credit Import Recherche). Pictures and
schema are welcomed.

